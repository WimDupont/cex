#include <sys/stat.h>
#include <sys/param.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <time.h>
#include <langinfo.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <errno.h>
#include <curses.h>

#include "config.h"

#define ctrl(x)		((x) & 0x1f)

typedef enum {OTHER, LEFT, RIGHT, UP, DOWN} Direction;
typedef enum {CURRENT, PARENT, CHILD} WinType;
typedef enum {COPY, REMOVE, MOVE} SelAction;

typedef struct _win_file {
	char d_name[256];
	unsigned char d_type;
	bool selected;
} WinFile;

typedef struct _dir_win {
	WinType wintype;
	WINDOW *window;
	WinFile *winfiles;
	char path[PATH_MAX];
	char *message;
	int maxx;
	int maxy;
	int startpr;
	int highlight;
	int filecount;
	bool holdupdate;
	bool usehighlight;
} DirWin;

static void init_dirwins(void);
static void init_screen(void);
static void resize(void);
static void start(void);
static void wpath(const char *filename);
static void reset_flags(void);
static void set_startpr(DirWin *dirwin);
static void set_win_files(DirWin *dirwin);
static void set_win_message(DirWin *dirwin, char *message);
static void print_win(DirWin *dirwin);
static void update_child_win(void);
static void print_top_title(void);
static void print_bot_title(void);
static void print_content(void);
static void show_file_mime(void);
static void select_file(const char *path);
static void exe_selection(SelAction action, const char *askn);
static void clear_selected(void);
static void clear_search(void);
static void combo_key(int keypress);
static void combo_go(int keypress);
static void combo_inf(int keypress);
static void combo_open(int keypress);
static void combo_make(int keypress);
static void change_dir(const char *chdname, Direction direction, bool abspath);
static void change_parent_dir(Direction direction);
static void open_child(bool exec);
static void open_nohup_xdg(void);
static void search(void);
static void move_top(DirWin *dirwin);
static void move_bot(DirWin *dirwin);
static void move_page_up(DirWin *dirwin);
static void move_page_down(DirWin *dirwin);
static void move_up(DirWin *dirwin);
static void move_down(DirWin *dirwin);
static void next_search(void);
static void prev_search(void);
static void free_dirwin(DirWin *dirwin);
static void run_command(size_t size, const char *fmt, ...);
static void clean(void);
static void fatal(const char *fmt, ...);
static bool is_dir(DirWin *dirwin, size_t index);
static bool is_selected(DirWin *dirwin, size_t count);
static bool remove_selected(const char *sel);
static bool prompt_confirm(size_t size, const char *fmt, ...);
static int compare_file(const void *a, const void *b);
static int get_mime(char *buf, size_t bufsize, const char *path);
static int get_mime_default(char *buf, size_t bufsize, const char *mime);
static int read_command(char *buf, size_t bufsize, const char *cmd);
static int make_file(const char *path);
static int make_dir(const char *path);
static int make_mod(void);
static int make_access(void);
static int rm_file(const char *fname);
static int rename_file(const char *fname);
static char *prompt_answer(char *buf, size_t size, const char *question);
static char *cpstr(char *dest, const char *src);
static char *get_file_info(char *buf, const char *filepath);
static char *get_fullpath(char *buf, DirWin *dirwin, size_t index);
static char *get_dirname(char *buf, const char *path);
static char *replace_home(char *buf, const char *path);

static DirWin curwin, parwin, childwin;
static char *userhome, *username, *editor, **selected, searchq[SEARCHLEN];
static int maxy, maxx;
static size_t selc, searchc;
static bool print_bot, hide = DEFAULT_HIDE;

int
main(int argc, char **argv)
{
	struct passwd userinf;
	char filename[PATH_MAX];
	int opt;
	bool writepath = FALSE;

	while ((opt = getopt(argc, argv, "f:")) != -1) {
		switch (opt) {
			case 'f':
				cpstr(filename, optarg);
				writepath = TRUE;
				break;
			default:
				fatal("Unknown option: %c\n", optopt);
				break;
		}
	}

	editor = getenv("EDITOR");
	
	userinf = *getpwuid(getuid());
	username = strdup(userinf.pw_name);
	userhome = strdup(userinf.pw_dir);

	init_dirwins();
	init_screen();

	start();

	if (writepath)
		wpath(filename);

	clean();
}

void
init_dirwins(void)
{
	curwin.wintype = CURRENT;
	parwin.wintype = PARENT;
	childwin.wintype = CHILD;

	reset_flags();

	childwin.highlight = 0;

	if (getcwd(curwin.path, PATH_MAX) == NULL)
		fatal("getcwd() error");

	get_dirname(parwin.path, curwin.path);

	set_win_files(&curwin);
	set_win_files(&parwin);

	if (curwin.winfiles != NULL && curwin.winfiles[0].d_type == DT_DIR) {
		cpstr(childwin.path, curwin.path);
		update_child_win();
	}
}

void
init_screen(void)
{
	initscr();
	noecho();
	start_color();
	use_default_colors();
	keypad(stdscr, TRUE);
	curs_set(0);

	clear();
	cbreak();

	init_pair(COLOR_BLACK, COLOR_BLACK, -1);
	init_pair(COLOR_RED, COLOR_RED, -1);
	init_pair(COLOR_GREEN,COLOR_GREEN, -1);
	init_pair(COLOR_YELLOW, COLOR_YELLOW, -1);
	init_pair(COLOR_BLUE, COLOR_BLUE, -1);
	init_pair(COLOR_MAGENTA, COLOR_MAGENTA, -1);
	init_pair(COLOR_CYAN, COLOR_CYAN, -1);
	init_pair(COLOR_WHITE, COLOR_WHITE, -1);

	curwin.highlight = 0;
	curwin.startpr = 0;

	parwin.highlight = 0;
	parwin.startpr = 0;

	childwin.highlight = 0;
	childwin.startpr = 0;

	resize();

	print_top_title();
	print_bot_title();

	print_win(&parwin);
	print_win(&curwin);
	print_win(&childwin);
}

void
resize(void)
{
	int startx;

	getmaxyx(stdscr, maxy, maxx);

	parwin.maxy = maxy-BORDER_SPACE_SIZE;
	parwin.maxx = maxx/4-BORDER_SPACE_SIZE > 0 ? maxx/4-BORDER_SPACE_SIZE : 1;
	startx = maxx > BORDER_SPACE_SIZE ? BORDER_SPACE_SIZE : 0;
	parwin.window = newwin(parwin.maxy, parwin.maxx, BORDER_SPACE_SIZE, startx);

	curwin.maxy = maxy-BORDER_SPACE_SIZE;
	curwin.maxx = maxx/4-BORDER_SPACE_SIZE > 0 ? maxx/4-BORDER_SPACE_SIZE : 1;
	startx = maxx > curwin.maxx+BORDER_SPACE_SIZE*2 ? curwin.maxx+BORDER_SPACE_SIZE*2 : 0;
	curwin.window = newwin(curwin.maxy, curwin.maxx, BORDER_SPACE_SIZE, startx);

	childwin.maxy = maxy-BORDER_SPACE_SIZE;
	childwin.maxx = maxx/2-BORDER_SPACE_SIZE > 0 ? maxx/2-BORDER_SPACE_SIZE : 1;
	startx = maxx > childwin.maxx+BORDER_SPACE_SIZE*2 ? childwin.maxx+BORDER_SPACE_SIZE*2 : 0;
	childwin.window = newwin(childwin.maxy, childwin.maxx, BORDER_SPACE_SIZE, startx);

	clear();
	refresh();

	update_child_win();
}

void
start(void)
{
	int c;
	char chdname[PATH_MAX];

	while ((c = getch()) != KEY_QUIT) {
		move(1, 1);
		clrtoeol();
		reset_flags();
		switch (c) {
			case KEY_UP:
			case KEY_VUP:
				move_up(&curwin);
				update_child_win();
				break;
			case KEY_DOWN:
			case KEY_VDOWN:
				move_down(&curwin);
				update_child_win();
				break;
			case KEY_VPUP:
				if (is_dir(&parwin, MAX(parwin.highlight-1, 0)))
					change_parent_dir(UP);
				break;
			case KEY_VPDOWN:
				if (is_dir(&parwin, MIN(parwin.highlight+1, parwin.filecount-1)))
					change_parent_dir(DOWN);
				break;
			case KEY_PPAGE:
			case ctrl('u'):
				move_page_up(&curwin);
				update_child_win();
				break;
			case KEY_NPAGE:
			case ctrl('d'):
				move_page_down(&curwin);
				update_child_win();
				break;
			case KEY_BOT:
				move_bot(&curwin);
				update_child_win();
				break;
			case 10:
			case KEY_VRIGHT:
			case KEY_VRIGHT_ABS:
				if (is_dir(&curwin, curwin.highlight))
					change_dir(get_fullpath(chdname, &curwin, curwin.highlight),
						RIGHT, c == KEY_VRIGHT_ABS);
				else
					open_child(FALSE);
				break;
			case KEY_LEFT:
			case KEY_VLEFT:
				change_dir(get_dirname(chdname, curwin.path), LEFT, FALSE);
				break;
			case KEY_SEARCH:
				search();
				update_child_win();
				break;
			case KEY_NEXT_SEARCH:
				next_search();
				update_child_win();
				break;
			case KEY_PREV_SEARCH:
				prev_search();
				update_child_win();
				break;
			case KEY_SEL_FILE:
				select_file(get_fullpath(chdname, &curwin, curwin.highlight));
				set_win_files(&curwin);
				update_child_win();
				break;
			case KEY_CLEAR_SEL:
				clear_selected();
				set_win_files(&curwin);
				set_win_files(&parwin);
				update_child_win();
				break;
			case KEY_CLEAR_SEARCH:
				clear_search();
				set_win_files(&curwin);
				break;
			case KEY_CP_SEL:
				exe_selection(COPY, NULL);
				break;
			case KEY_RM_SEL:
				exe_selection(REMOVE, "Remove");
				break;
			case KEY_MV_SEL:
				exe_selection(MOVE, "Move");
				break;
			case KEY_RM_FILE:
				rm_file(curwin.winfiles[curwin.highlight].d_name);
				set_win_files(&curwin);
				update_child_win();
				break;
			case KEY_RENAME_FILE:
				rename_file(curwin.winfiles[curwin.highlight].d_name);
				set_win_files(&curwin);
				update_child_win();
				break;
			case KEY_HIDE:
				hide = hide ? FALSE : TRUE;
				curwin.highlight = 0;
				curwin.startpr = 0;
				set_win_files(&curwin);
				set_win_files(&parwin);
				update_child_win();
				break;
			case KEY_COMBO_GO:
			case KEY_COMBO_INF:
			case KEY_COMBO_OPEN:
			case KEY_COMBO_MAKE:
				combo_key(c);
				break;
			case KEY_RESIZE:
				resize();
				break;
			default:
				break;
		}

		print_top_title();
		if (print_bot)
			print_bot_title();
		print_win(&parwin);
		print_win(&curwin);
		print_win(&childwin);
	}
}

void
wpath(const char *filename)
{
	FILE *fptr;

	if ((fptr = fopen(filename, "w")) == NULL)
		fatal("Error opening file \"%s\"\n", filename);
	fprintf(fptr, "%s\n", curwin.path);
	fclose(fptr);
}

void
reset_flags(void)
{
	parwin.usehighlight = FALSE;
	curwin.usehighlight = TRUE;
	childwin.usehighlight = TRUE;

	parwin.holdupdate = FALSE;
	curwin.holdupdate = FALSE;
	childwin.holdupdate = FALSE;

	print_bot = TRUE;
}

void
set_startpr(DirWin *dirwin)
{
	if (dirwin->winfiles == NULL) {
		dirwin->startpr = 0;
		return;
	}

	dirwin->startpr = MAX(dirwin->highlight - dirwin->maxy/2, 0);
}

void
set_win_files(DirWin *dirwin)
{
	struct dirent *ent;
	size_t count = 0;
	DIR *dir;

	free_dirwin(dirwin);

	if (dirwin->wintype == PARENT && strcmp(curwin.path, "/") == 0)
		return;

	if ((dir = opendir(dirwin->path)) == NULL) {
		switch (errno) {
			case EACCES:
				set_win_message(dirwin, "No permission.");
				return;
			default:
				fatal("Could not open directory: %s", dirwin->path);
		}
	}

	if ((dirwin->winfiles = (WinFile*) malloc(sizeof(WinFile))) == NULL)
		fatal("Fatal: failed to malloc.\n");

	while ((ent = readdir(dir)) != NULL) {
		if (strcmp(ent->d_name, ".") == 0
				|| strcmp(ent->d_name, "..") == 0
				|| (hide && ent->d_name[0] == '.'))
			continue;
		if ((dirwin->winfiles = (WinFile*) realloc(dirwin->winfiles, (count+1)*(sizeof(WinFile)))) == NULL)
			fatal("Fatal: failed to realloc.\n");
		cpstr(dirwin->winfiles[count].d_name, ent->d_name);
		dirwin->winfiles[count].d_type = ent->d_type;
		dirwin->winfiles[count].selected = is_selected(dirwin, count);
		(count)++;
	}
	closedir(dir);

	dirwin->filecount = count;

	if (count == 0)
		set_win_message(dirwin, "empty");
	else
		qsort(dirwin->winfiles, count, sizeof(WinFile), compare_file);
}

bool
is_selected(DirWin *dirwin, size_t index)
{
	char buf[PATH_MAX];

	if (selc == 0 || selected == NULL)
		return FALSE;

	get_fullpath(buf, dirwin, index);

	for (size_t i = 0; i < selc; i++) {
		if (strcmp(buf, selected[i]) == 0)
			return TRUE;
	}

	return FALSE;
}

bool
remove_selected(const char *sel)
{
	bool res = FALSE;

	if (selc == 0)
		return res;

	for (size_t i = 0; i < selc; i++) {
		if (strcmp(selected[i], sel) == 0) {
			free(selected[i]);
			selected[i] = selected[selc-1];
			res = TRUE;
			break;
		}
	}

	if (!res)
		return res;

	if (--(selc) == 0) {
		free(selected);
		selected = NULL;
	} else if ((selected = (char**) realloc(selected, selc * sizeof(char *))) == NULL)
		fatal("Fatal: failed to realloc.\n");

	return res;
}

void
set_win_message(DirWin *dirwin, char *message)
{
	free_dirwin(dirwin);
	dirwin->message = message;
}

void
print_win(DirWin *dirwin)
{
	if (dirwin->holdupdate)
		return;

	size_t cplen, size = dirwin->maxx;
	char *subs, name[size+1], sbuf[size+1], pathbuf[PATH_MAX];
	int sindex, y = 0, x = 1;

	wclear(dirwin->window);

	set_startpr(dirwin);

	if (dirwin->message != NULL) {
		if (dirwin->wintype == CURRENT) {
			wattron(dirwin->window, A_REVERSE);
			mvwaddstr(dirwin->window, y, x, dirwin->message);
			wattroff(dirwin->window, A_REVERSE);
		} else {
			wattron(dirwin->window, COLOR_PAIR(CHILDWIN_MESSAGE_COLOR));
			mvwaddstr(dirwin->window, y, x, dirwin->message);
			wattroff(dirwin->window, COLOR_PAIR(CHILDWIN_MESSAGE_COLOR));
		}
	} else if (dirwin->winfiles != NULL) {
		while (!dirwin->usehighlight) {
			for (size_t i = 0; i < dirwin->filecount; ++i) {
				memcpy(name, dirwin->winfiles[i].d_name, size);
				name[size] = '\0';
				if (strcmp(basename(curwin.path), dirwin->winfiles[i].d_name) == 0) {
					dirwin->usehighlight = TRUE;
					dirwin->highlight = i;
					set_startpr(dirwin);
					break;
				}
			}
			break;
		}
		for (size_t i = dirwin->startpr; i < dirwin->filecount; ++i) {
			mvwaddch(dirwin->window, y, x-1, ' ');
			if (dirwin->winfiles[i].selected) {
				wattron(dirwin->window, COLOR_PAIR(MARK_SELECTED_COLOR));
				mvwaddch(dirwin->window, y, x-1, '|');
				wattroff(dirwin->window, COLOR_PAIR(MARK_SELECTED_COLOR));
			}
			memcpy(name, dirwin->winfiles[i].d_name, size);
			name[size-1] = '\0';
			if (dirwin->usehighlight && dirwin->highlight == i) {
				wattron(dirwin->window, A_REVERSE);
				mvwaddstr(dirwin->window, y, x, name);
				wattroff(dirwin->window, A_REVERSE);
			} else if (dirwin->winfiles[i].d_type == DT_DIR) {
				wattron(dirwin->window, COLOR_PAIR(DIR_COLOR));
				wattron(dirwin->window, A_BOLD);
				mvwaddstr(dirwin->window, y, x, name);
				wattroff(dirwin->window, COLOR_PAIR(DIR_COLOR));
				wattroff(dirwin->window, A_BOLD);
			} else if (dirwin->winfiles[i].d_type == DT_LNK) {
				if (access(get_fullpath(pathbuf, dirwin, i), F_OK) == 0) {
					wattron(dirwin->window, COLOR_PAIR(LN_COLOR));
					mvwaddstr(dirwin->window, y, x, name);
					wattroff(dirwin->window, COLOR_PAIR(LN_COLOR));
				} else {
					wattron(dirwin->window, COLOR_PAIR(INVALID_LN_COLOR));
					mvwaddstr(dirwin->window, y, x, name);
					wattroff(dirwin->window, COLOR_PAIR(INVALID_LN_COLOR));
				}
			} else
				mvwaddstr(dirwin->window, y, x, name);
			if (dirwin->wintype == CURRENT && searchc > 0
					&& ((subs = strstr(name, searchq)) != NULL)) {
				sindex = (int) (subs - name);
				wattron(dirwin->window, COLOR_PAIR(SEARCH_MATCH_COLOR));
				if (dirwin->usehighlight && dirwin->highlight == i)
					wattron(dirwin->window, A_REVERSE);
				cplen = strlen(searchq);
				memcpy(sbuf, name + sindex, cplen);
				sbuf[cplen] = '\0';
				mvwaddstr(dirwin->window, y, x+sindex, sbuf);
				wattroff(dirwin->window, COLOR_PAIR(SEARCH_MATCH_COLOR));
				if (dirwin->usehighlight && dirwin->highlight == i)
					wattroff(dirwin->window, A_REVERSE);
			}
			y++;
		}
	}
	wrefresh(dirwin->window);
}

void
update_child_win(void)
{
	char pathbuf[PATH_MAX];

	if (curwin.filecount <= 0) {
		free_dirwin(&childwin);
		return;
	}

	get_fullpath(childwin.path, &curwin, curwin.highlight);

	switch (curwin.winfiles[curwin.highlight].d_type) {
		case DT_LNK:
			if (access(get_fullpath(pathbuf, &curwin, curwin.highlight), F_OK) != 0) {
				set_win_message(&childwin, "Link no longer exists.");
				break;
			}
			if (is_dir(&curwin, curwin.highlight))
				goto dir;
			else
				goto reg;
		case DT_DIR:
	dir:
			set_win_files(&childwin);
			break;
		case DT_REG:
	reg:
			print_content();
			break;
		default:
			free_dirwin(&childwin);
			break;
	}
}

void
print_top_title(void)
{
	move(0, 0);
	clrtoeol();
	attron(COLOR_PAIR(TOP_TITLE_COLOR));
	printw("%s:%s/%s", username, curwin.path, curwin.winfiles[curwin.highlight].d_name);
	attroff(COLOR_PAIR(TOP_TITLE_COLOR));
}

void
print_bot_title(void)
{
	char pathbuf[PATH_MAX], fileinf[maxx];

	get_fullpath(pathbuf, &curwin, curwin.highlight);
	memcpy(fileinf, get_file_info(fileinf, pathbuf), maxx);
	fileinf[maxx] = '\0';

	move(maxy-1, 0);
	clrtoeol();

	attron(COLOR_PAIR(BOT_TITLE_COUNT_COLOR));
	printw("(%d/%d) ", curwin.filecount == 0 ? 0 : curwin.highlight+1, curwin.filecount);
	attroff(COLOR_PAIR(BOT_TITLE_COUNT_COLOR));

	attron(COLOR_PAIR(BOT_TITLE_INFO_COLOR));
	addstr(fileinf);
	attroff(COLOR_PAIR(BOT_TITLE_INFO_COLOR));
}

char *
get_file_info(char *buf, const char *filepath)
{
	struct stat statbuf;
	struct passwd *usr;
	struct tm *tm;
	struct group *grp;
	char mods[11], datestr[256], modfill = '-';

	if ((stat(filepath, &statbuf)) != 0) {
		switch (errno) {
			case EACCES:
				buf = "No permission.";
				break;
			default:
				buf = "Retrieving filestats failed.";
				break;
		}
		return buf;
	}

	if ((usr = getpwuid(statbuf.st_uid)) == NULL) {
		buf = "Retrieving username failed.";
		return buf;
	}
	if ((grp = getgrgid(statbuf.st_gid)) == NULL) {
		buf = "Retrieving groupname failed.";
		return buf;
	}

	mods[0] = S_ISDIR(statbuf.st_mode) ? 'd' : modfill;
	mods[1] = statbuf.st_mode & S_IRUSR ? 'r' : modfill;
	mods[2] = statbuf.st_mode & S_IWUSR ? 'w' : modfill;
	mods[3] = statbuf.st_mode & S_IXUSR ? 'x' : modfill;
	mods[4] = statbuf.st_mode & S_IRGRP ? 'r' : modfill;
	mods[5] = statbuf.st_mode & S_IWGRP ? 'w' : modfill;
	mods[6] = statbuf.st_mode & S_IXGRP ? 'x' : modfill;
	mods[7] = statbuf.st_mode & S_IROTH ? 'r' : modfill;
	mods[8] = statbuf.st_mode & S_IWOTH ? 'w' : modfill;
	mods[9] = statbuf.st_mode & S_IXOTH ? 'x' : modfill;
	mods[10] = '\0';

	tm = localtime(&statbuf.st_mtime);
	strftime(datestr, sizeof(datestr), nl_langinfo(D_T_FMT), tm);

	sprintf(buf, "%10.10s %s %s %9jd %s",
		mods,
		usr->pw_name,
		grp->gr_name,
		(intmax_t)statbuf.st_size,
		datestr);

	return buf;
}

void
print_content(void)
{
	FILE *fp = NULL;
	size_t size = childwin.maxx;
	char str[size+1], buf[PATH_MAX];
	int y = 0, x = 0;

	werase(childwin.window);
	free_dirwin(&childwin);

	if ((fp = fopen(get_fullpath(buf, &curwin, curwin.highlight), "r")) == NULL) {
		switch (errno) {
			case EACCES:
				set_win_message(&childwin, "No permission.");
				return;
			default:
				fatal("Error opening file \"%s\"\n", buf);
		}
	}

	childwin.holdupdate = TRUE;

	while (fgets(str, size, fp) != NULL && y <= childwin.maxy) {
		mvwaddstr(childwin.window, y, x, str);
		y++;
	}

	fclose(fp);
	wrefresh(childwin.window);
}

void
show_file_mime(void)
{
	char buf[MIME_MAX];

	if (get_mime(buf, MIME_MAX, curwin.winfiles[curwin.highlight].d_name) != 0)
		cpstr(buf, "Can't retrieve mime.");

	move(1, 1);
	clrtoeol();
	addstr(buf);
}

void
combo_key(int keypress)
{
	int c;

	move(maxy-1, maxx-1);
	clrtoeol();
	addch(keypress);

	halfdelay(10);

	while ((c = getch()) != ERR) {
		switch (keypress) {
			case KEY_COMBO_GO:
				combo_go(c);
				break;
			case KEY_COMBO_INF:
				combo_inf(c);
				break;
			case KEY_COMBO_OPEN:
				combo_open(c);
				break;
			case KEY_COMBO_MAKE:
				combo_make(c);
				break;
			default:
				break;
		}
		break;
	}

	cbreak();
	move(maxy-1, maxx-1);
	clrtoeol();
}

void
combo_go(int key)
{
	char pathbuf[PATH_MAX];

	switch (key) {
		case KEY_COMBO_GO_TOP:
			move_top(&curwin);
			update_child_win();
			break;
		case KEY_COMBO_GO_HOME:
			change_dir(userhome, OTHER, FALSE);
			break;
		case KEY_COMBO_GO_ACCESS:
			change_dir(replace_home(pathbuf, LN_ACCESS_DIR), OTHER, FALSE);
			break;
		default:
			break;
	}
}

void
combo_inf(int key)
{
	switch (key) {
		case KEY_COMBO_INF_OPEN:
			show_file_mime();
			break;
		default:
			break;
	}
}

void
combo_open(int key)
{
	char chdname[PATH_MAX];

	switch (key) {
		case KEY_COMBO_OPEN_NOHUP_XDG:
			if (is_dir(&curwin, curwin.highlight))
				change_dir(get_fullpath(chdname, &curwin, curwin.highlight), RIGHT, FALSE);
			else
				open_nohup_xdg();
			break;
		case KEY_COMBO_OPEN_EXEC:
			if (is_dir(&curwin, curwin.highlight))
				change_dir(get_fullpath(chdname, &curwin, curwin.highlight), RIGHT, FALSE);
			else
				open_child(TRUE);
			break;
		default:
			break;
	}
}

void
combo_make(int key)
{
	switch (key) {
		case KEY_COMBO_MAKE_FILE:
			make_file(curwin.path);
			set_win_files(&curwin);
			update_child_win();
			break;
		case KEY_COMBO_MAKE_DIR:
			make_dir(curwin.path);
			set_win_files(&curwin);
			update_child_win();
			break;
		case KEY_COMBO_MAKE_MOD:
			make_mod();
			break;
		case KEY_COMBO_MAKE_ACCESS:
			make_access();
			break;
		default:
			break;
	}
}

void
change_dir(const char *chdname, Direction direction, bool abspath)
{
	if (chdir(chdname) != 0)
		return;

	if (!abspath)
		cpstr(curwin.path, chdname);
	else if (getcwd(curwin.path, PATH_MAX) == NULL)
		fatal("getcwd() error");

	switch (direction) {
		case RIGHT:
			curwin.highlight = 0;
			break;
		case LEFT:
			curwin.highlight = parwin.highlight;
			break;
		default:
			curwin.highlight = 0;
			break;

	}

	set_win_files(&curwin);

	get_dirname(parwin.path, curwin.path);
	set_win_files(&parwin);

	update_child_win();
}

void
change_parent_dir(Direction direction)
{
	char pp[PATH_MAX];

	switch (direction) {
		case UP:
			move_up(&parwin);
			break;
		case DOWN:
			move_down(&parwin);
			break;
		default:
			return;
	}

	parwin.usehighlight = TRUE;
	curwin.highlight = 0;
	change_dir(get_fullpath(pp, &parwin, parwin.highlight), direction, FALSE);
}

void
open_child(bool exec)
{
	sigset_t set;
	char mime[MIME_MAX], mimedefault[MIME_APP_MAX], pathbuf[PATH_MAX];
	bool istext;

	if (curwin.message != NULL)
		return;

	if (!exec) {
		if (get_mime(mime, MIME_MAX, curwin.winfiles[curwin.highlight].d_name) != 0) {
			move(1, 1);
			clrtoeol();
			printw("Can\'t open %s", curwin.winfiles[curwin.highlight].d_name);
			return;
		}

		istext = strncmp(mime, "text/", 5) == 0;

		if (!istext && get_mime_default(mimedefault, MIME_APP_MAX, mime) != 0) {
			move(1, 1);
			clrtoeol();
			printw("Can\'t open for mime %s", mime);
			return;
		}
	}

	endwin();

	if (sigemptyset (&set) == -1)
		fatal("Sigemptyset failed.");
	if (sigaddset(&set, SIGWINCH) == -1)
		fatal("Sigaddset failed.");
	if (sigprocmask(SIG_BLOCK, &set, NULL) != 0)
		fatal("Blocking sigprocmask failed.");

	if (exec)
		run_command(PATH_MAX + 4, "\"./%s\"", curwin.winfiles[curwin.highlight].d_name);
	else
		run_command(PATH_MAX + 12, "%s \"%s\"", istext ? editor : "xdg-open",
			get_fullpath(pathbuf, &curwin, curwin.highlight));

	if (sigprocmask(SIG_UNBLOCK, &set, NULL) != 0)
		fatal("Unblocking sigprocmask failed.");

	clear();
	noecho();
	cbreak();

	refresh();
	wrefresh(parwin.window);
	wrefresh(curwin.window);
	wrefresh(childwin.window);
	update_child_win();
}

void
open_nohup_xdg(void)
{
	char *cmdfmt = "nohup xdg-open \"%s\" > /dev/null 2>&1 &";
	size_t size = PATH_MAX + strlen(cmdfmt);

	run_command(size, cmdfmt, curwin.winfiles[curwin.highlight].d_name);
}

void
search(void)
{
	int c, y, x;

	clear_search();

	move(maxy-1, 0);
	clrtoeol();

	attron(COLOR_PAIR(PROMPT_COLOR));
	addstr("Search name? ");
	attroff(COLOR_PAIR(PROMPT_COLOR));

	echo();
	curs_set(1);

	getyx(stdscr, y, x);

	while ((c = mvwgetch(stdscr, y, x)) != 10) {
		switch (c) {
			case KEY_BACKSPACE:
				if (searchc > 0) {
					searchq[--(searchc)] = '\0';
					move(y, --x);
					clrtoeol();
				}
				break;
			default:
				if (searchc < SEARCHLEN) {
					searchq[(searchc)++] = c;
					move(y, ++x);
				}
				break;
		}
		print_win(&curwin);
	}
	noecho();
	curs_set(0);
}

void
move_top(DirWin *dirwin)
{
	dirwin->highlight = 0;
}

void
move_bot(DirWin *dirwin)
{
	dirwin->highlight = MAX(dirwin->filecount-1, 0);
}

void
move_page_up(DirWin *dirwin)
{
	dirwin->highlight = MAX(dirwin->highlight - dirwin->maxy/2, 0);
}

void
move_page_down(DirWin *dirwin)
{
	dirwin->highlight = MIN(dirwin->highlight + dirwin->maxy/2,
		MAX(dirwin->filecount-1, 0));
}

void
move_up(DirWin *dirwin)
{
	if (dirwin->highlight > 0)
		dirwin->highlight--;
}

void
move_down(DirWin *dirwin)
{
	if (dirwin->highlight < dirwin->filecount-1)
		dirwin->highlight++;
}

void
next_search(void)
{
	if (searchc == 0)
		return;

	for (size_t i = curwin.highlight; i < MAX(curwin.filecount-1, 0); i++) {
		if ((strstr(curwin.winfiles[i+1].d_name, searchq) != NULL)) {
			curwin.highlight = i+1;
			return;
		}
	}
}

void
prev_search(void)
{
	if (searchc == 0)
		return;

	for (size_t i = curwin.highlight; i > 0; i--) {
		if ((strstr(curwin.winfiles[i-1].d_name, searchq) != NULL)) {
			curwin.highlight = i-1;
			return;
		}
	}
}

void
free_dirwin(DirWin *dirwin)
{
	free(dirwin->winfiles);
	dirwin->filecount = 0;
	dirwin->winfiles = NULL;
	dirwin->message = NULL;
}

void
clean(void)
{
	wclear(curwin.window);
	wclear(parwin.window);
	wclear(childwin.window);

	free(username);
	free(userhome);

	clear();

	delwin(parwin.window);
	delwin(curwin.window);
	delwin(childwin.window);
	delwin(stdscr);

	free_dirwin(&curwin);
	free_dirwin(&parwin);
	free_dirwin(&childwin);

	clear_selected();

	endwin();
}

void
fatal(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	clean();

	exit(EXIT_FAILURE);
}

bool
is_dir(DirWin *dirwin, size_t index)
{
	struct stat statbuf;
	char linkpath[PATH_MAX], abspath[PATH_MAX];

	if (dirwin->filecount <= 0)
		return FALSE;

	if (dirwin->winfiles[index].d_type == DT_DIR)
		return TRUE;

	get_fullpath(linkpath, dirwin, index);

	if (dirwin->winfiles[index].d_type == DT_LNK)
		return (lstat(realpath(linkpath, abspath), &statbuf) >= 0) && S_ISDIR(statbuf.st_mode);

	return FALSE;
}

char *
prompt_answer(char *buf, size_t size, const char *question)
{
	move(maxy-1, 0);
	clrtoeol();

	attron(COLOR_PAIR(PROMPT_COLOR));
	addstr(question);
	attroff(COLOR_PAIR(PROMPT_COLOR));

	echo();
	curs_set(1);
	getnstr(buf, size);
	noecho();
	curs_set(0);

	return buf;
}

bool
prompt_confirm(size_t size, const char *fmt, ...)
{
	char response[1], question[size];
	va_list ap;

	va_start(ap, fmt);
	vsnprintf(question, size, fmt, ap);
	va_end(ap);

	prompt_answer(response, 1, question);

	return strcasecmp(response, "y") == 0;
}

int
compare_file(const void *a, const void *b)
{
	int typecompare;
	const WinFile *ma = a;
	const WinFile *mb = b;

	if ((typecompare = ma->d_type - mb->d_type) == 0)
		return strcmp(ma->d_name, mb->d_name);

	return typecompare;
}

int
get_mime(char *buf, size_t bufsize, const char *path)
{
	char *cmdfmt = "xdg-mime query filetype \"%s\"";
	size_t size = PATH_MAX + strlen(cmdfmt);
	char cmd[size];

	snprintf(cmd, size, cmdfmt, path);

	return read_command(buf, bufsize, cmd);
}

int
get_mime_default(char *buf, size_t bufsize, const char *mime)
{
	char *cmdfmt = "xdg-mime query default \"%s\"";
	size_t cmdsize = MIME_MAX + strlen(cmdfmt);
	char cmd[cmdsize];

	snprintf(cmd, cmdsize, cmdfmt, mime);

	return read_command(buf, bufsize, cmd);
}

int
read_command(char *buf, size_t bufsize, const char *cmd)
{
	FILE *file;

	if ((file = popen(cmd, "r")) == NULL) {
		buf = NULL;
		return 1;
	}

	if (fgets(buf, bufsize, file) == NULL) {
		buf = NULL;
		pclose(file);
		return 2;
	}

	buf[strlen(buf)-1] = '\0';

	pclose(file);

	return 0;
}

void
run_command(size_t size, const char *fmt, ...)
{
	va_list ap;
	char cmd[size];

	va_start(ap, fmt);
	vsnprintf(cmd, size, fmt, ap);
	va_end(ap);

	system(cmd);
}

int
make_dir(const char *path)
{
	char name[PATH_MAX];

	prompt_answer(name, PATH_MAX, "Name of directory? ");
	return mkdir(name, 0755);
}

int
make_file(const char *path)
{
	FILE *fptr;
	char name[PATH_MAX];

	prompt_answer(name, PATH_MAX, "Name of file? ");

	if (strlen(name) > 0) {
		if ((fptr = fopen(name, "w")) == NULL)
			fatal("Error opening file \"%s\"\n", name);
		fclose(fptr);
	}
}

int
rm_file(const char *fname)
{
	char *msg, *rmdirfmt = "rm -rf \"%s\"", *pfmt = "Remove %s? (y/N) ";
	int res = 0;
	size_t cmdsize = PATH_MAX + strlen(rmdirfmt), psize = strlen(pfmt) + strlen(fname);

	if (prompt_confirm(psize, pfmt, fname)) {
		if (is_dir(&curwin, curwin.highlight))
			run_command(cmdsize, rmdirfmt, fname);
		else
			res = remove(fname);
		curwin.highlight = MAX(curwin.highlight-1, 0);
	}

	if (res != 0) {
		switch (errno) {
			case EACCES:
				msg = "No permission.";
				break;
			case EEXIST:
			case ENOTEMPTY:
				msg = "Directory is not empty.";
				break;
			default:
				msg = "Could not remove file.";
				break;
		}
		move(maxy-1, 0);
		clrtoeol();
		addstr(msg);
		print_bot = FALSE;
	}

	return res;
}

int
rename_file(const char *fname)
{
	char name[PATH_MAX];

	prompt_answer(name, PATH_MAX, "New name? ");

	if (strlen(name) > 0)
		return rename(fname, name);

	return 0;
}

int
make_mod(void)
{
	char name[3];
	bool isvalid;

	prompt_answer(name, 3, "File mods (numeric)? ");

	if (strlen(name) == 3) {
		isvalid = TRUE;
		for (size_t i = 0; i < 3; i++) {
			if (!isdigit(name[i]))
				isvalid = FALSE;
		}
		if (isvalid)
			chmod(curwin.winfiles[curwin.highlight].d_name, strtol(name, NULL, 8));
		else
			return -1;
	}

	return 0;
}

int
make_access(void)
{
	char pathbuf[PATH_MAX], *cmdfmt = "ln -s \"%s\" \"%s\"", *pfmt = "Make access \"%s\"? (y/N) ";
	size_t cmdsize = PATH_MAX + strlen(cmdfmt), psize = strlen(pfmt) + PATH_MAX;

	if (prompt_confirm(psize, pfmt, curwin.winfiles[curwin.highlight].d_name))
		run_command(cmdsize, cmdfmt, get_fullpath(pathbuf, &curwin, curwin.highlight), LN_ACCESS_DIR);
}

void
exe_selection(SelAction action, const char *askn)
{
	char *pfmt = "%s selection (%d files) ? (y/N) ";
	size_t cmdsize = PATH_MAX*2 + 20;

	if (selected == NULL || selc == 0)
		return;

	if (askn != NULL && !prompt_confirm(strlen(pfmt) + strlen(askn) + 5, pfmt, askn, selc))
		return;

	switch (action) {
		case COPY:
			for (size_t i = 0; i <selc; i++)
				run_command(cmdsize, "cp -rf \"%s\" \"%s\"", selected[i], curwin.path);
			break;
		case REMOVE:
			for (size_t i = 0; i <selc; i++)
				run_command(cmdsize, "rm -rf \"%s\"", selected[i]);
			break;
		case MOVE:
			for (size_t i = 0; i <selc; i++)
				run_command(cmdsize, "mv \"%s\" \"%s\"", selected[i], curwin.path);
			break;
		default:
			break;
	}

	clear_selected();
	curwin.highlight = 0;
	set_win_files(&curwin);
	set_win_files(&parwin);
	update_child_win();
}

void
select_file(const char *path)
{
	if (selc == 0 && ((selected = (char**) malloc(sizeof(char *)))) == NULL)
		fatal("Fatal: failed to malloc selected.\n");

	if (remove_selected(path))
		return;

	if ((selected = (char**) realloc(selected, (selc+1) * sizeof(char *))) == NULL)
		fatal("Fatal: failed to realloc.\n");

	selected[selc] = strdup(path);
	(selc)++;
}

void
clear_selected(void)
{
	for (size_t i = 0; i < selc; i++)
		free(selected[i]);

	free(selected);
	selected = NULL;
	selc = 0;
}

void
clear_search(void)
{
	for (size_t i = 0; i < SEARCHLEN; i++)
		searchq[i] = '\0';
	searchc = 0;
}

char *
cpstr(char *dest, const char *src)
{
	size_t cplen = strlen(src);
	memcpy(dest, src, cplen);
	dest[cplen] = '\0';
	return dest;
}

char *
get_fullpath(char *buf, DirWin *dirwin, size_t index)
{
	index = MIN(index, dirwin->filecount-1);
	index = MAX(index, 0);

	if (strcmp(dirwin->path, "/") == 0)
		snprintf(buf, PATH_MAX, "/%s", dirwin->winfiles[index].d_name);
	else
		snprintf(buf, PATH_MAX, "%s/%s", dirwin->path, dirwin->winfiles[index].d_name);

	return buf;
}

char *
replace_home(char *buf, const char *path)
{
	char *envv = "$HOME";

	if ((strstr(path, envv) != NULL)) {
		cpstr(buf, userhome);
		strcat(buf, path + strlen(envv));
	}
	return buf;
}

char *
get_dirname(char *buf, const char *path)
{
	cpstr(buf, path);
	dirname(buf);
	return buf;
}
