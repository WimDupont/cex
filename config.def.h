#ifndef CONFIG_H
#define CONFIG_H

#define DEFAULT_HIDE TRUE
#define MIME_MAX 255
#define MIME_APP_MAX 255
#define BORDER_SPACE_SIZE 2
#define SEARCHLEN 20
#define LN_ACCESS_DIR "$HOME/access"

#define KEY_QUIT 'q'
#define KEY_VUP 'k'
#define KEY_VDOWN 'j'
#define KEY_VPUP 'K'
#define KEY_VPDOWN 'J'
#define KEY_VLEFT 'h'
#define KEY_VRIGHT 'l'
#define KEY_VRIGHT_ABS 'L'
#define KEY_BOT 'G'
#define KEY_NEXT_SEARCH 'n'
#define KEY_PREV_SEARCH 'N'
#define KEY_HIDE '.'
#define KEY_SEL_FILE ' '
#define KEY_CLEAR_SEL 'c'
#define KEY_CLEAR_SEARCH 'C'
#define KEY_MAKE_FILE 'f'
#define KEY_RM_FILE 'D'
#define KEY_RENAME_FILE 'r'
#define KEY_CP_SEL 'p'
#define KEY_RM_SEL 'd'
#define KEY_MV_SEL 'M'
#define KEY_SEARCH '/'

#define KEY_COMBO_GO 'g'
#define KEY_COMBO_GO_TOP 'g'
#define KEY_COMBO_GO_HOME 'h'
#define KEY_COMBO_GO_ACCESS 'a'

#define KEY_COMBO_INF 'i'
#define KEY_COMBO_INF_OPEN 'o'

#define KEY_COMBO_OPEN 'o'
#define KEY_COMBO_OPEN_EXEC 'x'
#define KEY_COMBO_OPEN_NOHUP_XDG 'o'

#define KEY_COMBO_MAKE 'm'
#define KEY_COMBO_MAKE_FILE 'f'
#define KEY_COMBO_MAKE_DIR 'd'
#define KEY_COMBO_MAKE_MOD 'm'
#define KEY_COMBO_MAKE_ACCESS 'a'

#define DIR_COLOR		COLOR_BLUE
#define LN_COLOR		COLOR_CYAN
#define INVALID_LN_COLOR	COLOR_RED
#define SEARCH_MATCH_COLOR	COLOR_YELLOW
#define TOP_TITLE_COLOR		COLOR_YELLOW
#define BOT_TITLE_COUNT_COLOR	COLOR_BLUE
#define BOT_TITLE_INFO_COLOR	COLOR_RED
#define PROMPT_COLOR		COLOR_MAGENTA
#define CHILDWIN_MESSAGE_COLOR	COLOR_MAGENTA
#define MARK_SELECTED_COLOR	COLOR_GREEN

#endif
