CC = gcc

NAME = cex
VERSION = 0.1

PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

BIN = cex
SRC = cex.c
OBJ = ${SRC:.c=.o}
MAN1 = ${BIN:=.1}

${OBJ}: config.h

all: ${BIN}

LDFLAGS=-lncurses

.c.o:
	$(CC) -g -c $(CFLAGS) $<

cex: ${OBJ}
	${CC} -g -o $@ ${OBJ} ${LDFLAGS}

config.h:
	cp config.def.h $@

clean:
	rm -f ${BIN} ${OBJ}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${BIN} "${DESTDIR}${PREFIX}/bin"
	chmod 755 "${DESTDIR}${PREFIX}/bin/${BIN}"
	mkdir -p "${DESTDIR}${MANPREFIX}/man1"
	sed "s/VERSION/${VERSION}/g" < ${MAN1} > "${DESTDIR}${MANPREFIX}/man1/${MAN1}"
	chmod 644 "${DESTDIR}${MANPREFIX}/man1/${MAN1}"

uninstall:
	rm -f \
		"${DESTDIR}${PREFIX}/bin/${BIN}"\
		"${DESTDIR}${MANPREFIX}/man1/${MAN1}"

.PHONY: all clean install uninstall
